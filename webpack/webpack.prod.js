const path = require('path');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = merge(common, {
  mode: 'production',
  entry: './src/index.ts',
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: 'vvdevAdminPanelUi.js'
  },
  plugins: [
    new CleanWebpackPlugin({ cleanStaleWebpackAssets: false })
  ],
  externals: {
    react: {
      commonjs: "React",
      commonjs2: "React",
      amd: "React",
      root: "React"
    },
    "styled-components": {
      commonjs: "styled",
      commonjs2: "styled",
      amd: "styled",
      root: "styled"
    }
  },
})
