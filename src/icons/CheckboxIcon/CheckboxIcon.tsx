import React from 'react';

const CheckboxIcon = (props: React.HtmlHTMLAttributes<SVGElement>) => {
  return (
    <svg {...props} viewBox='0 0 18 19'>
      <rect fill='#0066FF' height='18' rx='4' width='18' y='0.5' />
      <path
        clipRule='evenodd'
        d='M13.0765 6.53735C13.4701 6.92479 13.4751 7.55793 13.0877 7.95152L8.65798 12.4515C8.28552 12.8299 7.68231 12.8512 7.28404 12.5001L4.96373 10.4547C4.54944 10.0895 4.50966 9.45756 4.87487 9.04327C5.24009 8.62898 5.872 8.58919 6.28629 8.95441L7.89663 10.374L11.6624 6.54848C12.0498 6.1549 12.6829 6.14991 13.0765 6.53735Z'
        fill='white'
        fillRule='evenodd'
      />
    </svg>
  );
};

export default CheckboxIcon;
