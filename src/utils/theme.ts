export default {
  palette: {
    primary: {
      main: '#0066FF'
    },
    secondary: {
      main: '#CCCDD3'
    },
    border: {
      main: '#EAEEF1'
    }
  },
  text: {
    color: {
      main: '#212126',
      second: '#989FA6',
      third: '#686875'
    }
  }
};
