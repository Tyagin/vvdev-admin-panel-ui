export const clsx = (
  ...args: (string | { [key: string]: boolean })[]
): string => {
  let className = '';

  args.forEach(arg => {
    if (typeof arg === 'object') {
      Object.keys(arg).forEach(key => {
        if (arg[key]) {
          className += ` ${key}`;
        }
      });
    } else if (typeof arg === 'string') {
      className += ` ${arg}`;
    }
  });

  return className;
};

export const updateArray = <T>(array: T[], value: T, findByProp?: keyof T) => {
  const result = array.slice();

  let matchIndex: number;

  if (findByProp) {
    matchIndex = result.findIndex(a => a[findByProp] === value[findByProp]);
  } else {
    matchIndex = result.indexOf(value);
  }

  if (matchIndex !== -1) {
    result.splice(matchIndex, 1);
  } else {
    result.push(value);
  }

  return result;
};
