import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import Checkbox from './components/Checkbox';

const App = () => {
  const [checked, setChecked] = useState(false);

  return (
    <Checkbox
      checked={checked}
      disabled={checked}
      onChange={() => setChecked(!checked)}
    />
  );
};

ReactDOM.render(<App />, document.getElementById('root'));
