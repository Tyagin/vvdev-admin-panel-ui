import { CSSProperties } from 'react';

export type ICheckboxClasses = Partial<{
  root: string;
  input: string;
  icon: string;
}>;

export interface ICheckboxProps {
  value?: any;
  checked?: boolean;
  disabled?: boolean;
  className?: string;
  style?: CSSProperties;
  classes?: ICheckboxClasses;
  onChange?: () => void;
}
