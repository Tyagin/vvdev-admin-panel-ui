import React from 'react';
import styled, { css } from 'styled-components';
import CheckboxIcon from '../../icons/CheckboxIcon';
import { ICheckboxProps } from './interface';
import theme from '../../utils/theme';
import { clsx } from '../../utils/functions';

const CheckboxRoot = styled.div<{ checked?: boolean; disabled?: boolean }>`
  box-sizing: border-box;
  flex: none;
  display: inline-flex;
  width: 18px;
  height: 18px;
  cursor: ${p => (p.disabled ? 'default' : 'pointer')};

  svg {
    width: 100%;
    height: 100%;

    rect {
      fill: transparent;
      stroke: ${p => p.theme.palette.border.main};
    }
  }

  ${p => {
    if (p.disabled) {
      return css`
        svg {
          rect {
            fill: ${p.theme.palette.secondary.main};
            stroke: transparent;
          }
        }
      `;
    }

    if (p.checked) {
      return css`
        svg {
          rect {
            fill: ${p.theme.palette.primary.main};
            stroke: transparent;
          }
        }
      `;
    }

    return css`
      &:hover {
        svg {
          rect {
            fill: transparent;
            stroke: ${p.theme.palette.primary.main};
          }
        }
      }
    `;
  }}
`;

CheckboxRoot.defaultProps = {
  theme
};

const Checkbox = ({
  style,
  checked,
  disabled,
  onChange,
  className,
  classes = {}
}: ICheckboxProps) => {
  const handleChange = () => {
    if (!disabled) {
      onChange();
    }
  };

  return (
    <CheckboxRoot
      checked={checked}
      className={clsx(className, classes.root)}
      disabled={disabled}
      style={style}
      onClick={handleChange}
    >
      <CheckboxIcon className={classes.icon} />
    </CheckboxRoot>
  );
};

export default Checkbox;
